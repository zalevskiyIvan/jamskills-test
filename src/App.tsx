import React from "react";
import logo from "./logo.svg";
import "./App.css";

import { Switch, Route, Redirect } from "react-router-dom";
import Auth from "./Components/Auth/Auth";
import HomePage from "./Components/HomePage/HomePage";
import HolandTest from "./Components/HolandTest/HolandTest";
import UskTest from "./Components/UskTest/UskTest";
import GatbTest from "./Components/GatbTest/GatbTest";

function App() {
  const token = localStorage.getItem("accessToken");
  return (
    <div className="App">
      <Switch>
        <Route path="/hol/:id" render={() => <HolandTest />} />
        <Route path="/usk/:id" render={() => <UskTest />} />
        <Route path="/gatb-5/:id" render={() => <GatbTest />} />
        <Route path="/auth" render={() => <Auth />} />
        {!token && <Redirect to="/auth" />}
        <Route path="/home" render={() => <HomePage />} />
        <Route path="/" render={() => <HomePage />} />
      </Switch>
    </div>
  );
}

export default App;
