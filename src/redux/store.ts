import { applyMiddleware, combineReducers, createStore } from "redux";
import { authReducer } from "./reducers/authReducer";
import middleware from "redux-thunk";
import { quizzesReducer } from "./reducers/quizzesReducer";
const reducers = combineReducers({
  authReducer,
  quizzesReducer,
});

export const store = createStore(reducers, applyMiddleware(middleware));

export type RootState = ReturnType<typeof reducers>;

type properties<T> = T extends { [key: string]: infer U } ? U : never;
export type ActionTypes<T extends { [key: string]: (...args: any[]) => any }> =
  ReturnType<properties<T>>;
