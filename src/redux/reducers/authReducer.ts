import { Dispatch } from "react";
import {
  logInCredentials,
  signUpCredentials,
} from "../../Components/Auth/types";
import { authAPI } from "../API/API";
import { ActionTypes } from "../store";

const initialState = {
  isSignedUp: null as null | boolean,
  isLoggedIn: null as null | boolean,
  loading: null as boolean | null,
};
type stateType = typeof initialState;

export const authReducer = (
  state = initialState,
  action: actionType
): stateType => {
  switch (action.type) {
    case "SIGN_UP":
      return {
        ...state,
        isSignedUp: action.isSignedUp,
      };
    case "LOG_IN":
      return {
        ...state,
        isLoggedIn: action.isLoggedIn,
      };
    case "SET_LOADING":
      return {
        ...state,
        loading: action.loading,
      };
    default:
      return state;
  }
};
type actionType = ActionTypes<typeof actions>;
const actions = {
  signUp: (isSignedUp: boolean) => ({ type: "SIGN_UP", isSignedUp } as const),
  logIn: (isLoggedIn: boolean) => ({ type: "LOG_IN", isLoggedIn } as const),
  setLoading: (loading: boolean) => ({ type: "SET_LOADING", loading } as const),
};

//Thunks
export const signUpT = (data: signUpCredentials) => {
  return async (dispatch: Dispatch<actionType>) => {
    dispatch(actions.setLoading(true));
    const res = await authAPI.signUp(data);

    if (res.status === 201) {
      dispatch(actions.signUp(true));
    } else dispatch(actions.signUp(false));
    dispatch(actions.setLoading(false));
  };
};

export const logInT = (data: logInCredentials) => {
  return async (dispatch: Dispatch<actionType>) => {
    dispatch(actions.setLoading(true));
    const res = await authAPI.logIn(data);

    if (res.status === 200) {
      localStorage.setItem("accessToken", res.data?.token);
      dispatch(actions.logIn(true));
    } else dispatch(actions.logIn(false));
    dispatch(actions.setLoading(false));
  };
};
//Thunks
