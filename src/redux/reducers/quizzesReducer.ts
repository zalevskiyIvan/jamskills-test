import { Dispatch } from "react";
import { holAnswerType } from "../../Components/HolandTest/types";
import { quizzType } from "../../Components/HomePage/types";
import { uskAnwsers } from "../../Components/UskTest/types";
import { authAPI, quizzesAPI } from "../API/API";
import { ActionTypes } from "../store";

const initialState = {
  actualQuizzes: [] as quizzType[],
  error: null as boolean | null,
  loading: false,
};
type stateType = typeof initialState;

export const quizzesReducer = (
  state = initialState,
  action: actionType
): stateType => {
  switch (action.type) {
    case "SET_QUIZZES":
      return {
        ...state,
        actualQuizzes: [...state.actualQuizzes, action.quizz],
      };
    case "SET_ERROR":
      return {
        ...state,
        error: true,
      };
    case "SET_LOADING":
      return {
        ...state,
        loading: action.loading,
      };
    case "CLEAN_ACTUALL_QUIZZES":
      return {
        ...state,
        actualQuizzes: [],
      };
    default:
      return state;
  }
};
type actionType = ActionTypes<typeof actions>;
const actions = {
  setActualQuizzes: (quizz: quizzType) =>
    ({ type: "SET_QUIZZES", quizz } as const),
  setError: () => ({ type: "SET_ERROR" } as const),
  setLoading: (loading: boolean) => ({ type: "SET_LOADING", loading } as const),
  cleanActuallQuizzes: () => ({ type: "CLEAN_ACTUALL_QUIZZES" } as const),
};

//Thunks
export const getActualQuizzesT = () => {
  return async (dispatch: Dispatch<actionType>) => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      dispatch(actions.setLoading(true));

      const res = await quizzesAPI.getActualQuizzesKids(token);
      if (res.status === 200) {
        dispatch(actions.cleanActuallQuizzes());
        res.data.map(async (quizzKid) => {
          const res = await quizzesAPI.getQuizzes(quizzKid.id, token);
          res.data.map((quizz) => {
            quizz.deadline_at = quizzKid.deadline_at;
            quizz.hr = quizzKid.hr;
            quizz.invited_at = quizzKid.invited_at;
            quizz.testing_user = quizz.testing_user;
            dispatch(actions.setActualQuizzes(quizz));
          });
        });
      } else dispatch(actions.setError());
      dispatch(actions.setLoading(false));
    }
  };
};

export const setHolandAnswersT = (answers: holAnswerType[], testId: number) => {
  return async (dispatch: Dispatch<actionType>) => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      dispatch(actions.setLoading(true));

      const res = await quizzesAPI.setHolandAnswers(answers, testId, token);

      if (res.status !== 200) dispatch(actions.setError());

      dispatch(actions.setLoading(false));
    }
  };
};

export const setUskAnswersT = (answers: uskAnwsers[], testId: number) => {
  return async (dispatch: Dispatch<actionType>) => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      dispatch(actions.setLoading(true));

      const res = await quizzesAPI.setUskAnswers(answers, testId, token);

      if (res.status !== 200) dispatch(actions.setError());

      dispatch(actions.setLoading(false));
    }
  };
};

export const setGatbAnswersT = (
  correctAnswersCount: number,
  testId: number
) => {
  return async (dispatch: Dispatch<actionType>) => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      dispatch(actions.setLoading(true));

      const res = await quizzesAPI.setGatbAnswers(
        correctAnswersCount,
        testId,
        token
      );

      if (res.status !== 200) dispatch(actions.setError());

      dispatch(actions.setLoading(false));
    }
  };
};
//Thunks
