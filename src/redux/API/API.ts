import axios, { AxiosError, AxiosResponse } from "axios";
import {
  logInCredentials,
  signUpCredentials,
} from "../../Components/Auth/types";
import { holAnswerType } from "../../Components/HolandTest/types";
import { quizzesKidType, quizzType } from "../../Components/HomePage/types";
import { uskAnwsers } from "../../Components/UskTest/types";

const instanse = axios.create({
  baseURL: " https://api.jamskills.ml/api/",
  headers: {
    // "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  },
});

export const authAPI = {
  signUp: async (data: signUpCredentials): Promise<AxiosResponse<void>> => {
    return instanse
      .post("testingusers/registration", data)
      .catch((err) => err.response);
  },
  logIn: async (
    data: logInCredentials
  ): Promise<AxiosResponse<{ token: string }>> => {
    return instanse
      .post("testingusers/login", data)
      .catch((err) => err.response);
  },
};

export const quizzesAPI = {
  getActualQuizzesKids: async (
    // get all quizzes packs
    token: string
  ): Promise<AxiosResponse<quizzType[]>> => {
    return instanse
      .get(`testingusers/setquizzes`, {
        headers: { Authorization: `Bearer ${token}` },
      })

      .catch((err) => err.response);
  },
  getQuizzes: async (
    // get quizzes in specific kid
    id: number,
    token: string
  ): Promise<AxiosResponse<quizzType[]>> => {
    return instanse
      .get(`testingusers/setquizzes/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .catch((err) => err.response);
  },
  setHolandAnswers: async (
    answers: holAnswerType[],
    testId: number,
    token: string
  ): Promise<AxiosResponse<void>> => {
    return instanse
      .post(`testingusers/setquizzes/${testId}/hol/answers`, answers, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .catch((err) => err.response);
  },
  setUskAnswers: async (
    answers: uskAnwsers[],
    testId: number,
    token: string
  ): Promise<AxiosResponse<void>> => {
    return instanse
      .post(`testingusers/setquizzes/${testId}/usk/answers`, answers, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .catch((err) => err.response);
  },
  setGatbAnswers: async (
    correctAnswersCount: number,
    testId: number,
    token: string
  ): Promise<AxiosResponse<void>> => {
    return instanse
      .post(
        `testingusers/setquizzes/${testId}/gatb_5/answers`,
        { result: correctAnswersCount },
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .catch((err) => err.response);
  },
};
