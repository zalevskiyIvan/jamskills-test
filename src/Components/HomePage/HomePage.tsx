import {
  FileDoneOutlined,
  LogoutOutlined,
  UserOutlined,
} from "@ant-design/icons";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useTypedSelector } from "../../hooks";
import { getActualQuizzesT } from "../../redux/reducers/quizzesReducer";
import style from "./HomePageStyles.module.css";
import dateFormat from "dateformat";
import { quizzStatuses, testTypes } from "./types";
import { Button, Input, Progress, Table } from "antd";
import { quizzType } from "./types";
import { useHistory } from "react-router";

const { Search } = Input;

export default function HomePage() {
  const history = useHistory();
  const dispatch = useDispatch();
  const quizzes = useTypedSelector(
    (state) => state.quizzesReducer.actualQuizzes
  );

  useEffect(() => {
    dispatch(getActualQuizzesT());
  }, []);

  const isLoading = useTypedSelector((state) => state.quizzesReducer.loading);
  const isError = useTypedSelector((state) => state.quizzesReducer.error);

  const goToQuizz = (id: number, testType: testTypes) => {
    history.push(`/${testType}/${id}`);
  };

  //Table settings

  const columns = [
    {
      title: "Тест",
      dataIndex: "quizz",
    },
    {
      title: "Подтест",
      dataIndex: "underQuizz",
    },
    {
      title: "Отправитель",
      dataIndex: "hr",
      render: (text: string) => <a href="https://mail.google.com">{text}</a>,
    },
    {
      title: "Приглашение",
      dataIndex: "invited_at",
      render: (text: string) => dateFormat(text),
    },
    {
      title: "Завершение",
      dataIndex: "deadline_at",
      render: (text: string) => dateFormat(text),
    },
    {
      title: "Состояние",
      dataIndex: "status",
    },
    {
      title: "Прогресс",
      dataIndex: "progress",
      render: (number: number) => <Progress percent={number} />,
    },
    {
      title: "Действия",
      dataIndex: "actions",
      render: (arr: any) => {
        const quizzId: number = arr[0];
        const status: quizzStatuses = arr[1];
        const testType = arr[2];
        if (status !== "completed") {
          return (
            <>
              <a onClick={() => goToQuizz(quizzId, testType)}>Перейти |</a>
              <a> Отказ |</a>
              <span> Результаты</span>
            </>
          );
        }
        if (status === "completed") {
          return (
            <>
              <span>Перейти |</span>
              <span> Отказ |</span>
              <a> Результаты</a>
            </>
          );
        }
      },
    },
  ];

  const data: any = [];

  quizzes.map((el) => {
    data.push({
      underQuizz: `Тест ${el.id}`,
      quizz: el.quiz,
      hr: el.hr,
      invited_at: el.invited_at,
      deadline_at: el.deadline_at,
      status: el.status,
      progress: 30,
      actions: [el.id, el.status, el.quiz],
    });
  });

  //

  return (
    <div className={style.home_page}>
      {/* {quizzes.map((el) => {
        return <h1 key={el.id}>{el.quiz}</h1>;
      })} */}
      <header>
        <img src="/images/jam_logo2.png" alt="" />
        <h2>Jamskills</h2>
      </header>
      <div className={style.navbar}>
        <div className={style.navbar_home}>
          <img src="/images/home_icon.png" alt="" />
          <h3>Домашняя страница</h3>
        </div>

        <div className={style.navbar_fooer}>
          <h3>Настройки</h3>
          <div className={style.icon_group}>
            <UserOutlined />
            <span>Профиль</span>
          </div>
          <div className={style.icon_group}>
            <LogoutOutlined />
            <span>Выход</span>
          </div>
        </div>
      </div>
      <div className={style.second_header}>
        <span>Домашняя страница</span>
        <h1>Домашняя страница</h1>
        <FileDoneOutlined />
      </div>
      <div className={style.main}>
        <div className={style.info}>
          <div className={style.info_block1}>
            <div className={style.info_title}>
              <div className={style.circle_one}></div>
              <span>Всего приглашений</span>
            </div>
            <h3>333</h3>
          </div>
          <div className={style.info_block2}>
            <div className={style.info_title}>
              <div className={style.circle_two}></div>
              <span>Пройдено</span>
            </div>
            <div className={style.info_data}>
              <img src="/images/like_icon.png" alt="" />
              <h3>33</h3>
            </div>
          </div>
          <div className={style.info_block3}>
            <div className={style.info_title}>
              <div className={style.circle_three}></div>
              <span>В процессе</span>
            </div>
            <div className={style.info_data}>
              <img src="/images/clock_icon.png" alt="" />

              <h3>22</h3>
            </div>
          </div>
          <div className={style.info_block4}>
            <div className={style.info_title}>
              <div className={style.circle_four}></div>
              <span>Отказано</span>
            </div>
            <div className={style.info_data}>
              <img src="/images/fire_icon.png" alt="" />

              <h3>23</h3>
            </div>
          </div>
        </div>
        <Search
          className={style.search}
          placeholder="input search text"
          style={{ width: 335 }}
        />

        <div className={style.table_block}>
          <div style={{ marginBottom: 16 }}></div>
          <Table
            size="small"
            pagination={false}
            columns={columns}
            dataSource={data}
          />
        </div>
      </div>
    </div>
  );
}
//! На главной странице выводить наборы или тесты?
