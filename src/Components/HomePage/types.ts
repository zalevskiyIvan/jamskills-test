export type quizzStatuses = "in_progress" | "completed" | "created" | "expired";

export type testTypes = "hol" | "usk" | "gatb-5";

export type quizzType = {
  quiz: string;
  status: quizzStatuses;
  testing_user: string;
  invited_at: string;
  deadline_at: string;
  id: number;
  hr: string;
};
export type quizzesKidType = {
  status: quizzStatuses;
  testing_user: string;
  invited_at: string;
  deadline_at: string;
  id: number;
  hr: string; // HR email
};
