import { Button, Form, Radio } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router";
import { useTypedSelector } from "../../hooks";
import { setHolandAnswersT } from "../../redux/reducers/quizzesReducer";
import CustomProgress from "../CustomProgress";
import style from "./HolandTestStyles.module.css";
import { holAnswerType } from "./types";

export default function HolandTest() {
  const dispatch = useDispatch();
  const history = useHistory();
  const testId = useParams<{ id: string }>().id;

  const holandQuestions = [
    {
      index: 1,
      question: "Какую профессию вы бы предпочли?",
      answers: {
        left: {
          name: "R",
          text: "Автомеханик",
        },
        right: {
          name: "I",
          text: "Биофизик",
        },
      },
    },
    {
      index: 2,
      question: "Какую профессию вы бы предпочли?",
      answers: {
        left: {
          name: "R",
          text: "Егерь",
        },
        right: {
          name: "S",
          text: "Интервьюер",
        },
      },
    },
    {
      index: 3,
      question: "Какую профессию вы бы предпочли?",
      answers: {
        left: {
          name: "R",
          text: "Кондитер",
        },
        right: {
          name: "C",
          text: "Делопроизводитель",
        },
      },
    },
  ];

  const [currentQuestion, setCurrentQuestion] = useState(1);

  const currentTask = holandQuestions[currentQuestion - 2];

  const [answers, setAnswers] = useState([] as holAnswerType[]);

  const completeTest = () => {
    dispatch(setHolandAnswersT(answers, +testId));

    localStorage.removeItem("currentQuestion");
    localStorage.removeItem("answers");

    debugger;
    history.push("/");
  };

  const setAnswer = (answer: string, index: number) => {
    if (answers.length !== 3) {
      setAnswers([...answers, { name: answer, index }]);
      setCurrentQuestion((oldCurrent) => oldCurrent + 1);

      if (currentQuestion !== 4)
        localStorage.setItem("currentQuestion", `${currentQuestion + 1}`);
    }
  };
  useEffect(() => {
    if (answers.length !== 0) {
      localStorage.removeItem("answers");
      localStorage.setItem("answers", JSON.stringify(answers));
    }

    if (answers.length === 3) {
      completeTest();
    }
  }, [answers.length]);

  useEffect(() => {
    const lkCurrentQ = localStorage.getItem("currentQuestion");
    if (lkCurrentQ) setCurrentQuestion(+lkCurrentQ);

    const lkAnswers: any = localStorage.getItem("answers");
    if (lkAnswers) setAnswers(JSON.parse(lkAnswers.split(",")));
  }, []);

  return (
    <div className={style.holand}>
      <header>
        <img src="/images/jam_logo2.png" alt="" />
        <div className={style.progress}>
          <CustomProgress
            stepsCount={holandQuestions.length}
            currentStep={currentQuestion}
          />
        </div>
      </header>
      <div className={style.test_content}>
        {currentQuestion === 1 && (
          <>
            <h1>Тест №{testId}</h1>
            <span>
              Вам попарно будут представлены различные профессии, например:
            </span>
            <div className={style.options}>
              <Button>ЗООТЕХНИК</Button>
              <Button>ГЛАВНЫЙ ВРАЧ</Button>
            </div>
            <span>
              В каждой паре Вам следует отдать предпочтение какой-то одной.
            </span>
            <p>
              Выбрать вариант можно при помощи мыши. Подтвердить выбор можно
              через двойной щелчок или кнопки “Продолжить”.
            </p>
            <Button
              onClick={() => setCurrentQuestion((oldCurrent) => oldCurrent + 1)}
              type="primary"
              className={style.allright}
            >
              Все понятно!
            </Button>
          </>
        )}

        {currentQuestion !== 1 && answers.length !== holandQuestions.length && (
          <div>
            <h1>Какую профессию вы бы предпочли?</h1>
            <Form
              onFinish={({ answer }) => setAnswer(answer, currentTask.index)}
            >
              <div>
                <Form.Item name="answer" rules={[{ required: true }]}>
                  <Radio.Group className={style.options}>
                    <Radio.Button value={currentTask.answers.left.name}>
                      {currentTask.answers.left.text}
                    </Radio.Button>
                    <Radio.Button value={currentTask.answers.right.name}>
                      {currentTask.answers.right.text}
                    </Radio.Button>
                  </Radio.Group>
                </Form.Item>
              </div>

              <Button
                htmlType="submit"
                type="primary"
                className={style.allright}
              >
                {currentQuestion === 4 ? "Завершить" : "Продолжить"}
              </Button>
            </Form>
          </div>
        )}
      </div>
    </div>
  );
}
