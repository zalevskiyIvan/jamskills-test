import { Button, Progress } from "antd";
import Countdown from "antd/lib/statistic/Countdown";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { setGatbAnswersT } from "../../redux/reducers/quizzesReducer";
import CustomProgress from "../CustomProgress";
import style from "./GatbTestStyles.module.css";

export default function GatbTest() {
  const dispatch = useDispatch();
  const history = useHistory();

  const testId = useParams<{ id: string }>().id;

  const [currentPictureId, setCurrentPicrureId] = useState(1);

  const [correctAnswers, setCorrectAnswers] = useState(0);

  const [currentQuestion, setCurrentQuestion] = useState(1);

  const makeChoice = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const id = e.currentTarget.id.split("_")[1];
    if (+id === currentPictureId) {
      const check: any = document.getElementById(e.currentTarget.id)
        ?.children[0];
      check.style.opacity = 1;
      setTimeout(() => {
        check.style.opacity = 0;
        setCurrentPicrureId(currentPictureId + 1);
      }, 1000);

      setCorrectAnswers(correctAnswers + 1);
    }
  };

  function onFinish() {
    dispatch(setGatbAnswersT(correctAnswers, +testId));
    history.push("/");
  }
  useEffect(() => {
    if (currentPictureId === 26) {
      dispatch(setGatbAnswersT(correctAnswers, +testId));
      history.push("/");
    }
  }, [currentPictureId]);

  const deadline = Date.now() + 1000 * 60 * 2;

  useEffect(() => {}, [deadline]);
  return (
    <div className={style.gatb}>
      <header>
        <div>
          <img src="/images/jam_logo2.png" alt="" />
        </div>
        <div className={style.progress}>
          <CustomProgress stepsCount={1} currentStep={currentQuestion} />
        </div>
        {currentQuestion !== 1 && (
          <div className={style.countdown}>
            <Progress
              type="circle"
              percent={100}
              format={() => {
                return (
                  <Countdown
                    format="mm:ss"
                    value={deadline}
                    onFinish={onFinish}
                  />
                );
              }}
            />
          </div>
        )}
      </header>
      <div className={style.test_content}>
        {currentQuestion === 1 && (
          <>
            <h1>Тест №{testId}</h1>
            <p>
              Вам будут представлены два множества, в которых представлены
              геометрические фигуры..
            </p>
            <span className={style.time_warning}>Этот тест на время!</span>
            <p>
              Найдите кажной фигуре первого множества соответствующую фигуру из
              второго.
            </p>
            <div className={style.helper_sign}>
              Выбрать вариант можно при помощи мыши. Подтвердить выбор можно
              через двойной щелчок или кнопки “Продолжить”.
            </div>
            <Button
              onClick={() => setCurrentQuestion((oldCurrent) => oldCurrent + 1)}
              type="primary"
              className={style.allright}
            >
              Все понятно!
            </Button>
          </>
        )}
        {currentQuestion !== 1 && (
          <>
            <h1>Найдите соответствующие фигуры в обоих множествах</h1>
            <div className={style.pictures}>
              <div className={style.task_block}>
                <img src="/images/gatb_1.png" alt="" />
                <div className={style.aria_l_1} id="1">
                  <img
                    style={
                      currentPictureId === 1 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_2} id="2">
                  <img
                    style={
                      currentPictureId === 2 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_3} id="3">
                  <img
                    style={
                      currentPictureId === 3 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_4} id="4">
                  <img
                    style={
                      currentPictureId === 4 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_5} id="5">
                  <img
                    style={
                      currentPictureId === 5 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_6} id="6">
                  <img
                    style={
                      currentPictureId === 6 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_7} id="7">
                  <img
                    style={
                      currentPictureId === 7 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_8} id="8">
                  <img
                    style={
                      currentPictureId === 8 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_9} id="9">
                  <img
                    style={
                      currentPictureId === 9 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_10} id="10">
                  <img
                    style={
                      currentPictureId === 10 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_11} id="11">
                  <img
                    style={
                      currentPictureId === 11 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_12} id="12">
                  <img
                    style={
                      currentPictureId === 12 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_13} id="13">
                  <img
                    style={
                      currentPictureId === 13 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_14} id="14">
                  <img
                    style={
                      currentPictureId === 14 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_15} id="15">
                  <img
                    style={
                      currentPictureId === 15 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_16} id="16">
                  <img
                    style={
                      currentPictureId === 16 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_17} id="17">
                  <img
                    style={
                      currentPictureId === 17 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_18} id="18">
                  <img
                    style={
                      currentPictureId === 18 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_19} id="19">
                  <img
                    style={
                      currentPictureId === 19 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_20} id="20">
                  <img
                    style={
                      currentPictureId === 20 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_21} id="21">
                  <img
                    style={
                      currentPictureId === 21 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_22} id="22">
                  <img
                    style={
                      currentPictureId === 22 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_23} id="23">
                  <img
                    style={
                      currentPictureId === 23 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_24} id="24">
                  <img
                    style={
                      currentPictureId === 24 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
                <div className={style.aria_l_25} id="25">
                  <img
                    style={
                      currentPictureId === 25 ? { opacity: 1 } : { opacity: 0 }
                    }
                    src="/images/check.png"
                    alt=""
                  />
                </div>
              </div>
              <div className={style.task_block}>
                <img className={style.task2} src="/images/gatb_2.png" alt="" />
                <div
                  className={style.aria_r_1}
                  onClick={(e) => makeChoice(e)}
                  id="right_1"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_2}
                  onClick={(e) => makeChoice(e)}
                  id="right_2"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_3}
                  onClick={(e) => makeChoice(e)}
                  id="right_3"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_4}
                  onClick={(e) => makeChoice(e)}
                  id="right_4"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_5}
                  onClick={(e) => makeChoice(e)}
                  id="right_5"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_6}
                  onClick={(e) => makeChoice(e)}
                  id="right_6"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_7}
                  onClick={(e) => makeChoice(e)}
                  id="right_7"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_8}
                  onClick={(e) => makeChoice(e)}
                  id="right_8"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_9}
                  onClick={(e) => makeChoice(e)}
                  id="right_9"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_10}
                  onClick={(e) => makeChoice(e)}
                  id="right_10"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_11}
                  onClick={(e) => makeChoice(e)}
                  id="right_11"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_12}
                  onClick={(e) => makeChoice(e)}
                  id="right_12"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_13}
                  onClick={(e) => makeChoice(e)}
                  id="right_13"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_14}
                  onClick={(e) => makeChoice(e)}
                  id="right_14"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_15}
                  onClick={(e) => makeChoice(e)}
                  id="right_15"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_16}
                  onClick={(e) => makeChoice(e)}
                  id="right_16"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_17}
                  onClick={(e) => makeChoice(e)}
                  id="right_17"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_18}
                  onClick={(e) => makeChoice(e)}
                  id="right_18"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_19}
                  onClick={(e) => makeChoice(e)}
                  id="right_19"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_20}
                  onClick={(e) => makeChoice(e)}
                  id="right_20"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_21}
                  onClick={(e) => makeChoice(e)}
                  id="right_21"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_22}
                  onClick={(e) => makeChoice(e)}
                  id="right_22"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_23}
                  onClick={(e) => makeChoice(e)}
                  id="right_23"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_24}
                  onClick={(e) => makeChoice(e)}
                  id="right_24"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
                <div
                  className={style.aria_r_25}
                  onClick={(e) => makeChoice(e)}
                  id="right_25"
                >
                  <img style={{ opacity: 0 }} src="/images/check.png" alt="" />
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
}
