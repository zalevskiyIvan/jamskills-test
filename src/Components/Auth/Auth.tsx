import React, { useEffect, useState } from "react";
import style from "./AuthStyles.module.css";

import { UserOutlined, MailOutlined, LockOutlined } from "@ant-design/icons";

import { Form, Input, Button, Checkbox } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { logInCredentials, signUpCredentials } from "./types";
import { logInT, signUpT } from "../../redux/reducers/authReducer";
import { useTypedSelector } from "../../hooks";
import { useHistory } from "react-router";

export default function Auth() {
  const history = useHistory();

  const isLoggedIn = useTypedSelector((state) => state.authReducer.isLoggedIn);
  const isSignedUp = useTypedSelector((state) => state.authReducer.isSignedUp);
  const isLoading = useTypedSelector((state) => state.authReducer.loading);

  const [pageMode, setPageMode] = useState("LOGIN" as "LOGIN" | "SIGNUP");
  const dispatch = useDispatch();
  const changePageMode = () => {
    setPageMode((oldMode) => (oldMode === "LOGIN" ? "SIGNUP" : "LOGIN"));
  };

  const signUp = ({
    email,
    last_name,
    first_name,
    first_password,
    second_password,
    patronymic,
  }: signUpCredentials) => {
    if (first_password === second_password) {
      dispatch(
        signUpT({
          email,
          last_name,
          first_name,
          patronymic,
          password: first_password,
        })
      );
    }
  };
  const logIn = async (data: logInCredentials) => {
    dispatch(logInT(data));
  };
  useEffect(() => {
    if (isLoggedIn === true) history.push("/");
  }, [isLoggedIn, isLoading]);

  useEffect(() => {
    if (isSignedUp === true && isLoading === false) setPageMode("LOGIN");
  }, [isSignedUp, isLoading]);

  return (
    <div className={style.auth_page}>
      <div className={style.sidebar}>
        <img src="images/jam_logo.png" alt="" />
        <h1>Добро пожаловать</h1>
        <p>
          Jamskills - это сервис для автоматизации оценки <br /> сотрудников и
          кандидатов!
        </p>
        <div className={style.help_sign}>
          {pageMode === "LOGIN" ? (
            <p>
              Если Вы еще не зарегистрированны, <br /> создайте свой кабинет
            </p>
          ) : (
            <p>
              Если Вы уже зарегистрированны, <br /> войдите в свой кабинет
            </p>
          )}
        </div>
        <button onClick={changePageMode}>
          {pageMode === "LOGIN" ? "Создать" : "Войти"}
        </button>
      </div>
      <div className={style.main}>
        {pageMode === "SIGNUP" ? (
          <>
            <Form className={style.form} onFinish={signUp}>
              <h1>Регистрация</h1>
              <div className={style.block_1}>
                <div className={style.user_icon}>
                  <UserOutlined />
                </div>
                <div className={style.block_1_fields}>
                  <Form.Item
                    className={style.last_name}
                    rules={[
                      { required: true, message: "This field may not empty" },
                    ]}
                    name="last_name"
                  >
                    <Input placeholder="Фамилия" />
                  </Form.Item>
                  <Form.Item
                    className={style.first_name}
                    rules={[
                      { required: true, message: "This field may not empty" },
                    ]}
                    name="first_name"
                  >
                    <Input placeholder="Имя" />
                  </Form.Item>
                  <Form.Item className={style.patronymic} name="patronymic">
                    <Input placeholder="Отчество" />
                  </Form.Item>
                </div>
              </div>
              <div className={style.email}>
                <MailOutlined />
                <Form.Item
                  rules={[
                    { required: true, message: "This field may not empty" },
                  ]}
                  name="email"
                >
                  <Input placeholder="example@mail.com" />
                </Form.Item>
              </div>
              <div className={style.password_group}>
                <LockOutlined />
                <div>
                  <Form.Item
                    rules={[
                      { required: true, message: "This field may not empty" },
                    ]}
                    name="first_password"
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    className={style.password_field}
                    name="second_password"
                    rules={[
                      { required: true, message: "This field may not empty" },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </div>
              </div>
              <div className={style.acception}>
                <Form.Item name="acception">
                  <Checkbox />
                </Form.Item>
                <p>
                  Принимаю {""}
                  <a href="#">
                    Пользовательское соглашение и Политику конфиденциальности
                  </a>
                </p>
              </div>
              <Button type="primary" htmlType="submit">
                Создать
              </Button>
            </Form>
            {isSignedUp === false && (
              <span style={{ color: "red" }}>Error</span>
            )}
          </>
        ) : (
          <>
            <Form className={style.form} onFinish={logIn}>
              <div className={style.login}>
                <h1>Авторизация</h1>
                <div className={style.email}>
                  <MailOutlined />
                  <Form.Item
                    rules={[
                      { required: true, message: "This field may not empty" },
                    ]}
                    name="email"
                  >
                    <Input placeholder="example@mail.com" />
                  </Form.Item>
                </div>
                <div className={style.login_password}>
                  <LockOutlined />
                  <Form.Item
                    rules={[
                      { required: true, message: "This field may not empty" },
                    ]}
                    name="password"
                  >
                    <Input placeholder="" />
                  </Form.Item>
                </div>
              </div>
              <Button type="primary" htmlType="submit">
                Войти
              </Button>
            </Form>
            {isLoggedIn === false && (
              <span style={{ color: "red" }}>Error</span>
            )}
          </>
        )}
      </div>
    </div>
  );
}
// {
//   "email": "test@example.com",
//   "password": "123456768",
//   "first_name": "Иван",
//   "last_name": "Иванов",
//   "patronymic": "Иванович"
// }
