export type signUpCredentials = {
  email: string;
  first_name: string;
  last_name: string;
  patronymic: string;
  first_password?: string;
  second_password?: string;
  password?: string;
};
export type logInCredentials = {
  email: string;
  password: string;
};
