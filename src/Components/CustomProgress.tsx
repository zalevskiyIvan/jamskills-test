import React, { useState } from "react";
import { Steps } from "antd";

export default function CustomProgress({
  stepsCount,
  currentStep,
}: {
  stepsCount: number;
  currentStep: number;
}) {
  const { Step } = Steps;
  const arr = [];
  for (let i = -1; i < stepsCount; i++) {
    arr.push(i);
  }

  return (
    <Steps size="small" current={currentStep - 1}>
      {arr.map((el) => {
        return <Step>{el}</Step>;
      })}
    </Steps>
  );
}
