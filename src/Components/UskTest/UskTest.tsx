import { Button, Form, Radio } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { useParams } from "react-router-dom";
import { useTypedSelector } from "../../hooks";
import { setUskAnswersT } from "../../redux/reducers/quizzesReducer";
import CustomProgress from "../CustomProgress";
import { uskAnwsers } from "./types";
import style from "./UskTestStyles.module.css";

export default function UskTest() {
  const history = useHistory();
  const dispatch = useDispatch();

  const isLoading = useTypedSelector((state) => state.quizzesReducer.loading);
  const isError = useTypedSelector((state) => state.quizzesReducer.error);

  const testId = useParams<{ id: string }>().id;

  const uskQuestions = [
    {
      index: 1,
      question:
        "Продвижение по службе больше зависит от удачного обстоятельства, чем от способностей и усилий человека.",
    },
    {
      index: 2,
      question:
        "Большинство разводов происходит оттого, что люди не захотели приспособиться друг к другу.",
    },
    {
      index: 3,
      question:
        "Болезнь – дело случая; если уж суждено заболеть, то ничего не поделаешь.",
    },
  ];
  const [currentQuestion, setCurrentQuestion] = useState(1);

  const currentTask = uskQuestions[currentQuestion - 2];

  const [answers, setAnswers] = useState([] as uskAnwsers[]);

  const completeTest = () => {
    dispatch(setUskAnswersT(answers, +testId));

    localStorage.removeItem("answers");
    localStorage.setItem("currentQuestion", "1");

    history.push("/");
  };

  const setAnswer = (code: string, index: number) => {
    if (answers.length !== 3) {
      setAnswers([...answers, { code, index }]);
      setCurrentQuestion((oldCurrent) => oldCurrent + 1);

      if (currentQuestion !== 4)
        localStorage.setItem("currentQuestion", `${currentQuestion + 1}`);
    }
  };

  useEffect(() => {
    if (answers.length !== 0) {
      localStorage.removeItem("answers");
      localStorage.setItem("answers", JSON.stringify(answers));
    }
    if (answers.length === 3) {
      completeTest();
    }
  }, [answers.length]);
  useEffect(() => {
    const lkCurrentQ = localStorage.getItem("currentQuestion");
    if (lkCurrentQ) setCurrentQuestion(+lkCurrentQ);

    const lkAnswers: any = localStorage.getItem("answers");
    if (lkAnswers) setAnswers(JSON.parse(lkAnswers.split(",")));
  }, []);

  return (
    <div className={style.usk}>
      <header>
        <img src="/images/jam_logo2.png" alt="" />
        <div className={style.progress}>
          <CustomProgress
            stepsCount={uskQuestions.length}
            currentStep={currentQuestion}
          />
        </div>
      </header>
      <div>
        <h1>Тест №{testId}</h1>
        {currentQuestion === 1 && (
          <div className={style.test_content}>
            <p>
              Вам предстоит оценить ряд утверждений, касающихся различных сторон
              жизни. Просим вас отвечать правдиво и быстро.
            </p>
            <p>
              Помните, что нет ответов хороших и плохих, правильных и
              неправильных. Просто мы все разные.
            </p>
            <div className={style.helper_sign}>
              Выбрать вариант можно при помощи мыши. Подтвердить выбор можно
              через двойной щелчок или кнопки “Продолжить”.
            </div>
            <Button
              onClick={() => setCurrentQuestion((oldCurrent) => oldCurrent + 1)}
              type="primary"
              className={style.allright}
            >
              Все понятно!
            </Button>
          </div>
        )}
        {currentQuestion !== 1 && answers.length !== uskQuestions.length && (
          <Form onFinish={({ answer }) => setAnswer(answer, currentTask.index)}>
            <div className={style.question_block}>
              <div className={style.question}>
                <p>{currentTask.question}</p>
              </div>
              <div className={style.options}>
                <Form.Item name="answer" rules={[{ required: true }]}>
                  <Radio.Group>
                    <Radio.Button value={"2"}>Полностью согласен</Radio.Button>
                    <Radio.Button value={"1"}>Пожалуй, согласен</Radio.Button>
                    <Radio.Button value={"0"}>
                      Скорее согласен, чем несогласен
                    </Radio.Button>
                    <Radio.Button value={"0"}>
                      Скорее несогласен, чем согласен
                    </Radio.Button>
                    <Radio.Button value={"-1"}>
                      Пожалуй, несогласен
                    </Radio.Button>

                    <Radio.Button value={"-2"}>
                      Полностью несогласен
                    </Radio.Button>
                  </Radio.Group>
                </Form.Item>
              </div>
            </div>
            <Button htmlType="submit" type="primary" className={style.allright}>
              {currentQuestion === 4 ? "Завершить" : "Продолжить"}
            </Button>
          </Form>
        )}
      </div>
    </div>
  );
}
